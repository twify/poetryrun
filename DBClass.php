<?php


class DB
{

    static $connection = NULL;

    public function __construct($host = '127.0.0.1', $user = 'root', $pass = '', $db = 'poetry_run')
    {
        if (is_null(self::$connection)) {
            $dsn = 'mysql:dbname=' . $db . ';host=' . $host;
            try {
                self::$connection = new PDO($dsn, $user, $pass);
                self::$connection->exec("SET NAMES UTF8");
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }
        }

        return self::$connection;
    }

    public static function prepareExecution($sql, array $values = NULL, array $options = array())
    {
        if (count($options) == 0) {
            // these options are for using :var and they are the default options
            $options = array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY);
        }
        $sth = self::$connection->prepare($sql, $options);

        $execute = $sth->execute($values);
        if ($execute != TRUE) {
            throw new \PDOException("Execution of SQL got wrong! SQL: " . $sql .
                " Values: " . ( count($values) > 0 ? implode(',', $values) : ''));
        }

        // from PDO::prepare returns - PDOStatement object, FALSE or PDOException
        return $sth;
    }

    public static function fetchAll($sql, array $values = NULL, $options = array(), $return = NULL)
    {
        if ( is_null($return) ) {
            $return = ( PDO::FETCH_ASSOC );
        }
        $sth = self::prepareExecution($sql, $values, $options);
        $result = $sth->fetchAll($return);
        return ( $result != FALSE ? $result : array() );
    }

    public static function fetchOne($sql, $values = NULL, $options = array(), $return = NULL)
    {
        if( is_null($return) ) {
            $return = (PDO::FETCH_ASSOC);
        }
        $sth = self::prepareExecution($sql, $values, $options);
        $result = $sth->fetch($return);
        return ( $result != FALSE ? $result : array());
    }
}
