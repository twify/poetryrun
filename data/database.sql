CREATE DATABASE poetry_run;
USE poetry_run;

CREATE TABLE words(
  id int(100) NOT NULL AUTO_INCREMENT,
  word varchar(255) UNIQUE NOT NULL,

  PRIMARY KEY (id)
);

CREATE TABLE rhymes(
  id int(200) NOT NULL AUTO_INCREMENT,

  fk_word1_id int(100) NOT NULL,
  fk_word2_id int(100) NOT NULL,

  KEY (fk_word1_id, fk_word2_id),
  CONSTRAINT `key_rhymes_fk_word1_id` FOREIGN KEY (`fk_word1_id`) REFERENCES `words` (`id`),
  CONSTRAINT `key_rhymes_fk_word2_id` FOREIGN KEY (`fk_word2_id`) REFERENCES `words` (`id`),

  PRIMARY KEY (id)
);

CREATE TABLE tournaments(
  id  int(100) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  started DATETIME NOT NULL,
  ended DATETIME NULL,

  PRIMARY KEY (id)
);

CREATE TABLE tournament_rhymes(
  fk_tour_id int(100) NOT NULL,
  fk_rhymes int(200) NOT NULL,

  PRIMARY KEY (fk_tour_id, fk_rhymes),

  CONSTRAINT `key_trhymes_tour_id` FOREIGN KEY (`fk_tour_id`) REFERENCES `tournaments` (`id`),
  CONSTRAINT `key_trhymes_rhymes` FOREIGN KEY (`fk_rhymes`) REFERENCES `rhymes` (`id`)

);

CREATE TABLE user_rhymes(
  id int(100) NOT NULL AUTO_INCREMENT,
  ip varchar(15) NOT NULL,
  fk_tour_id int(100) NOT NULL,
  text text NOT NULL,

  PRIMARY KEY (id),
  KEY (fk_tour_id),

  CONSTRAINT `key_urhymes_tour_id` FOREIGN KEY (`fk_tour_id`) REFERENCES `tournaments` (`id`)
);

CREATE TABLE votes(
  id int(100) NOT NULL AUTO_INCREMENT,
  ip varchar(15) NOT NULL,
  fk_user_rhyme_id int(100) NOT NULL,

  value tinyint(1) NOT NULL DEFAULT 1,

  PRIMARY KEY (id),
  KEY (fk_user_rhyme_id),

  CONSTRAINT `key_votes_user_rhyme_id` FOREIGN KEY (`fk_user_rhyme_id`) REFERENCES `user_rhymes` (`id`)
);

