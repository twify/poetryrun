<?php
/**
 * Created by: elpiel
 * Project: poetryrun
 * 25.01.2014
 */


// Configure
include "include/config.php";

if (isset($_POST['poem'])) {
    $poem = $_POST['poem'];
    $ip = getIp();
    $poem_clear = clear_text($poem);

    //$select_last = "SELECT * FROM user_rhymes WHERE ip='" . getIp() . "' AND fk_tour_id='1' ORDER BY id DESC";
    //$user = $db_connection->fetchOne($select_last);
    //$user['has_played'] = (count($user) > 0 ? TRUE : FALSE);

    $insert_sql = "INSERT INTO user_rhymes (ip, fk_tour_id, text) VALUES (:ip, :tour_id, :text)";
    $values = array(':ip' => $ip, ':tour_id' => 1, ':text' => $poem_clear);

    DB::prepareExecution($insert_sql, $values);

    /*if (!$user['has_played']) {

    } else {

    }*/
} else {
    header("Location: index.php");
}
$text = "Вашият стих е записан успешно за играта. Следващата игра ще е на 28.01.2014";


// Including the header of the page
include "include/header.php";

//if (!$user['has_played']) {
    ?>
    <div class="alert alert-danger">
        <table border="0" style="vertical-align: top; border-collapse: separate; border-spacing: 5px;">
            <tr>
                <td width="300">
                    <img src="images/red_gameover.png" style="width: 100%; height: 300px;"/>
                </td>
                <td valign="top">
                    <h3><?= $text; ?></h3><br><h4>Вашият стих е:</h4>
                    <hr/>
                    <p>
                        <?= nl2br($poem_clear); ?>
                    </p>
                </td>
            </tr>
        </table>
    </div>
<?php
/*}else{
    header("Location: index.php");
}*/


// including footer
include "include/footer.php";