<!DOCTYPE html>
<html>
<head>
    <title>Poetry Run</title>

    <meta name="Googlebot" content="index,follow" />
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="1 Days" />

    <meta charset="UTF-8">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/page.css">
    <link rel="stylesheet" href="css/flipclock.css">
    <link href="css/ui-lightness/jquery-ui-1.10.4.custom.css" rel="stylesheet">

    <link rel="shortcut icon" href="pr_16x16_new.ico">

    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui-1.10.4.custom.js"></script>


    <script src="js/flipclock/libs/base.js"></script>
    <script src="js/flipclock/flipclock.js"></script>
    <script src="js/flipclock/faces/hourlycounter.js"></script>
    <script src="js/flipclock/faces/minutecounter.js"></script>

    <script src="js/bootstrap.js"></script>
</head>
<body style="background-image: url('images/bg8.png'); background-size: 40 cover; background-repeat:repeat;">

<div class="container whitebg wrap">
    <header class="center-block">
        <img src="images/head3.png" class="center-block" id="image">
        <!-- My navigation bar for going around the website -->
        <nav class="navbar navbar-static-top" style="padding-left: 35%; margin-top: -35px;">
            <ul class="nav nav-pills">
                <li <?=(empty($current) ? 'class="active"' : ''); ?>><a href="index.php">Включи се и ти</a></li>
                <!--<li><a href="#">Гласувай</a></li>-->
                <li <?=($current == 'results' ? 'class="active"' : ''); ?>><a href="results.php">Резултати</a></li>
            </ul>
        </nav>
    </header>
