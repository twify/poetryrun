<?php
/**
 * Created by: elpiel
 * Project: poetryrun
 * 25.01.2014
 */


function __autoload ($class_name)
{
    include $class_name . 'Class.php';
}

function getIp ()
{
    if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) //if from shared
    {
        return $_SERVER['HTTP_CLIENT_IP'];
    } else if ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) //if from a proxy
    {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        return $_SERVER['REMOTE_ADDR'];
    }
}

function clear_text($text)
{
    $breaks = array( "<br />", "<br>", "<br/>" );
    $text = str_replace($breaks, "\r\n", $text);

    $text = strip_tags($text, '<b>');
    return $text;
}

// starting the Database Connection
$db_connection = new DB( 'localhost', 'poetry', 'poetry1234' );

$current = "";
