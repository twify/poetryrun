<?php

/**
 * Created by: elpiel
 * Project: poetryrun
 * 26.01.2014
 */

include "include/config.php";

// Including the header of the page
include "include/header.php";



/*
 * $insert_sql = "INSERT INTO agent_" . $this->agent_name . " (situation, action, q_value) VALUES (:situation, :action, :q_value)";
        $values = array( ':situation' => $situation, ':action' => $action, 'q_value' => $q_value );

        return DB::prepareExecution($insert_sql, $values);

$select_situation = "SELECT * FROM agent_" . $this->agent_name . "
        WHERE situation=:situation AND action=:action
        LIMIT 0,1";
        $values = array( ':situation' => $situation, ':action' => $action );

        return DB::fetchOne($select_situation, $values);
 *
 *
 */

$all_words = $db_connection->fetchAll("SELECT word FROM words");
$words = array();
foreach ($all_words as $word) {
    $words[] = $word['word'];
}

/*foreach ($all_words as $word) {
    $words[] = $word['word'];
}

$sql = "SELECT
              (SELECT w.word FROM words w WHERE r.fk_word1_id=w.id ) as word_one,
              (SELECT w.word FROM words w WHERE r.fk_word2_id=w.id ) as word_two,
              tour.name,
              tour.id
              FROM tournament_rhymes trhymes
              INNER JOIN tournaments tour ON tour.id=trhymes.fk_tour_id
              LEFT JOIN rhymes r ON ( trhymes.fk_rhymes = r.id )

            WHERE tour.id='1'";
*/


$sql = "SELECT
              (SELECT w.word FROM words w WHERE r.fk_word1_id=w.id ) as word_one,
              (SELECT w.word FROM words w WHERE r.fk_word2_id=w.id ) as word_two
              FROM rhymes r
              ORDER BY RAND()";

//$tour = $db_connection->fetchOne("SELECT tour.id, tour.name FROM tournaments tour");

$all_rhymes = $db_connection->fetchAll($sql);
$rhymes = array();
foreach ($all_rhymes as $word_ar) {
    $rhymes[] = array($word_ar['word_one'], $word_ar['word_two']);
}
//$user = $db_connection->fetchOne("SELECT * FROM user_rhymes WHERE ip='". getIp()."' AND fk_tour_id='1'");
//$user['has_played'] = (count($user) > 0 ? TRUE : FALSE);

//var_dump(count($user));
//var_dump($user['has_played']);

//$user['has_played'] = true;
/*$user['text'] = "

";*/


//$title = "Игра #".$tour['id']." - ".$tour['name'];

$title = "Историята за \"".$all_rhymes[0]['word_one']."\"";

?>
    <script>

        var rows = [
            [],
            []
        ];
        var poem = [];

        var flip_clock = null;

        var rhymes = <?= json_encode($rhymes); ?>;

        var rhymes_current = 0;
        var row_current = 0; // first row

        var start_seconds = 60;
        var time_spacing = 2;

        $(function () {

            //clock_init(start_seconds);

            $("#poem_hidden").hide();
            $("#word_one").text(rhymes[0][0]);
            $("#word_two").text(rhymes[0][1]);

            var availableTags = <?= json_encode($words); ?>;
            $("#autocomplete").autocomplete({
                source: function (request, response) {
                    var matches = $.map(availableTags, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });

                    response(matches.slice(0, 11));
                }
            }).keydown(function (event) {
                    var value = $(this).val();
                    if (event.keyCode == 13) {
                        $("#save_row").click();
                        return false;
                    }

                    if (event.keyCode == 32) {
                        //$(this).val(value.substr(0,value.length-1));
                        add_word();

                        return false;
                    }
                });

            // Hover states on the static widgets
            $("#dialog-link, #icons li").hover(
                function () {
                    $(this).addClass("ui-state-hover");
                },
                function () {
                    $(this).removeClass("ui-state-hover");
                }
            );
        });

        function clock_init (time) {

            if( flip_clock != null ) {
                //flip_clock_cancel = true;
                flip_clock.setTime(time);
            }else{
                flip_clock = $('.timer').FlipClock(time, {
                    clockFace: 'MinuteCounter',
                    countdown: true,
                    callbacks: {
                        stop: function () {
                            $("#form").submit();
                        }
                    }
                });
            }

            return flip_clock;
        }

        function add_word() {

            var found = false;

            if( jQuery.inArray( $("#autocomplete").val(), <?=json_encode($words); ?>) != -1 ) {
                found = true;
            }

            if( found == true ) {
                var field = $("#autocomplete");
                var word = field.val();
                rows[row_current].push(word);
                field.val("");
            }

            show_words();
            console.log("Word added: \"" + word + "\"");
        }

        function show_words() {
            $("#row-" + row_current).text("");
            rows[row_current].forEach(function (word) {
                $("#row-" + row_current).append(" " + word);
            });

        }

        function remove_word() {
            rows[row_current].splice(rows[row_current].length - 1, 1);
            show_words();
        }

        function save_to_poem() {
            if (rows[row_current].length > 0) {
                $("#row-" + row_current).removeClass("active-row");

                if( row_current == 0 ){
                    row_current++;
                }else{
                    row_current = 0;

                    var word_one = $("#word_one");
                    var word_two = $("#word_two");

                    // adding first word to the poem
                    rows[0].push(word_one.text());
                    poem.push(rows[0]);

                    //adding second word to the poem
                    rows[1].push(word_two.text());
                    poem.push(rows[1]);

                    rows = [
                        [],
                        []
                    ];
                    $("#row-0").text("");
                    $("#row-1").text("");

                    show_poem();

                    console.log('rhyme current:' + rhymes_current);


                    if( rhymes.length-1 == rhymes_current ) {
                        //$("#poem_hidden").val($("#poem").text());

                        //console.log($("#poem").text());

                        console.log($("#poem_hidden").val());
                        $("#form").submit();
                    }else{
                        rhymes_current++;

                        if (rhymes_current > 0) {
                            // (rhymes_current-1)*
                            if ( rhymes_current > 1) {
                                start_seconds -= time_spacing;
                            }


                            clock_init(start_seconds);
                        }


                        //console.log(rhymes[rhymes_current][0]);
                        //console.log(rhymes[rhymes_current][1]);

                        word_one.text(rhymes[rhymes_current][0]);
                        word_two.text(rhymes[rhymes_current][1]);
                    }
                }
                $("#row-" + row_current).addClass("active-row");

                $("#error_word").text("");
            } else {
                $("#error_word").text("Моля въведете поне една дума за реда");
            }


        }

        function show_poem() {
            $("#poem").text("");
            poem.forEach(function (row) {
                row.forEach(function (word, index) {

                    if (index == row.length - 1) {
                        $("#poem").append(" <b>" + word + ",</b><br>");
                    } else {
                        $("#poem").append(" " + word);
                    }

                });
            });
            $("#poem_hidden").val($("#poem").html());
        }

    </script>

    <!-- START BOX CONTENT -->
    <section class="center-block" style="height: 500px; padding-left: 15px;">

        <div>
            <h2 class="page-header"><?=$title; ?></h2>
        </div>

        <form action="submit_result.php" id="form" method="post">
            <textarea name="poem" id="poem_hidden"></textarea>
        </form>
    <?php
        /*if ($user['has_played'] ) {
?>
            <div class="alert alert-info">
                <h3>За тази игра вие вече сте участвали! Следващата игра ще е на 28.01.2014 г.</h3><br>
                <h4>Вашият стих е:</h4>
                <hr />
                <p>
                    <?=nl2br($user['text']); ?>
                </p>
            </div>

<?php
        //}else{ */
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row" style="width: 500px;">

                <div class="col-md-7" id="poem" style="height: auto;"></div>
                <div class="col-md-5">&nbsp;</div>

                <div class="col-md-7 active-row" id="row-0"></div>

                <div class="col-md-3" id="word_one"></div>
                <div class="col-md-2">
                    &nbsp;
                </div>

                <div class="col-md-7" id="row-1"></div>
                <div class="col-md-3" id="word_two"></div>
                <div class="col-md-2">
                    <button type="submit" onclick="remove_word();" class="btn btn-primary">Изтрий дума</button>
                </div>

                <div class="col-md-7">
                    <input id="autocomplete" class="form-control" id="inputEmail" placeholder="Дума"
                           title="type &quot;a&quot;" autofocus>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary" onclick="add_word();">Добави думата</button>
                </div>
                <div class="col-md-2">
                    <button type="submit" id="save_row" onclick="save_to_poem();" class="btn btn-primary">Запиши стих</button>
                </div>

                <div class="col-md-12" style="color: red;" id="error_word"></div>

                <div class="col-md-12">
                    <div class="alert alert-info" style="height: auto;">
                        <ol>
                            <li>С Интервал добавяте думата към текущия ред.</li>
                            <li>С Enter записвате съответния ред</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="timer" style="height: 150px;"></div>
                <div class="alert alert-warning" style="height: auto;">
                    <h3 class="text-center">Как да играем</h3>
                    <p>
                        Получаваш две римуващи се думи.<br>
                        <br>
                        За кратък период от време трябва да напишеш два стиха.<br>
                        След това се генерират друга двойка римуващи се думи.<br>
                        <br>
                        Направи най-дълга поема за ограниченото време.
                    </p>
                </div>
        </div>


    </div>
<?php
        //}
?>


        <!-- <div class="ui-state-default ui-corner-all"></div> -->
<?php
// Including the header of the page
include "include/footer.php";
