<?php

/**
 * Created by: elpiel
 * Project: poetryrun
 * 26.01.2014
 */

include "include/config.php";

$current = "results";

// Including the header of the page
include "include/header.php";


$users_poems = $db_connection->fetchAll("SELECT * FROM user_rhymes ORDER BY id DESC");
$current_row = 0;

$divs_in_row = array('', '');

foreach ($users_poems as $poem) {
/*
 * <div class="panel panel-info" style="height: auto;">
            <div class="panel-heading">Поема #'.$poem['id'].'</div>
 */

    $divs_in_row[$current_row] .= '<div class="panel panel-info" style="height: auto;">
            <div class="panel-heading">Поема #'.$poem['id'].'</div>
            <div class="panel-body">'.nl2br($poem['text']).'</div>
        </div>';
    if( $current_row == 1 ) {
        $current_row = 0;
    }else{
        $current_row++;
    }

}
?>

    <!--<div class="col-md-6">
        <div class="panel panel-info" style="height: auto;">
            <div class="panel-heading">Поема #'.$poem['id'].'</div>
            <div class="panel-body">'.nl2br($poem['text']).'</div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info" style="height: auto;">
            <div class="panel-heading"\n <br>>Поема #'.$poem['id'].'</div>
            <div class="panel-body">'.nl2br($poem['text']).'</div>
        </div>
    </div>-->
<section class="center-block" style="height: 100%; width: 80%;">
    <table style="width: 100%; border-spacing: 5px; border-collapse: separate;">
        <tr>
            <td style="width: 50%; vertical-align: top;">
                <?=$divs_in_row[0];?>
            </td>
            <td style="vertical-align: top;">
                <?=$divs_in_row[1];?>
            </td>
        </tr>
    </table>
</section>
<?php
// Including the header of the page
include "include/footer.php";


